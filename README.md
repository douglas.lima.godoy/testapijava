# TestApiJava

Este projeto contém testes automatizados para validar o comportamento de uma API de testes, usando Java e diversas ferramentas.

## Requisitos
- Java JDK 17.0.10 ou superior
- Maven 3.x

## Configurações:
- Dependências: rest-assured 4.0.0, junit 4.13.1
- A estrutura do projeto contém uma interface chamada "Constant.java" onde as contantes do projeto estão armazenadas, na "BaseTest.java" a configuração do projeto está definida na classe "setup()" e os métodos de teste estão no package= "test". 
 

- Para rodar o projeto é necessário executar cada classe de teste de forma indivudual.


- Instalar o java 21
- Instalar o Apache Maven 4.0.0
- IDE utilizada: IntellJ 2023.3.2

## Relatórios
Os relatórios dos testes são gerados automaticamente e podem ser encontrados no diretório /allure-results.
