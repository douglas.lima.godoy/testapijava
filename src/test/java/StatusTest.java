import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import br.testApi.BaseTest;

public class StatusTest extends BaseTest{

    //Test to validate application status
    @Test
    public  void testStatus(){

        given()
                .when()
                    .get("/test")
                .then()
                    .statusCode(200)
                    .body("status", equalTo("ok"))
                    .body("method", equalTo("GET"))
                    .statusCode(200)
                    .body("status", equalTo("ok"))
                    .body("method", equalTo("GET"))
                    .statusCode(200)
                    .body("status", equalTo("ok"))
                    .body("method", equalTo("GET"));
    }
}
