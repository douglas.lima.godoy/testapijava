import br.testApi.BaseTest;
import org.junit.Test;


import java.util.List;
import java.util.Random;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;


public class ProductsTest extends BaseTest {
    LoginTest loginSuccessfully = new LoginTest();

    //Test to find products with correct token
    @Test
    public void searchProductsWithAuthentication() {

        given()
                .contentType("application/json")
                .header("Authorization", "Bearer " + loginSuccessfully.loginSuccessfully());
        when()
                .get("/auth/products")
        .then()
                .statusCode(200)
                .body("products", hasSize(greaterThan(0)))
                .body("products[0].id", notNullValue());
        System.out.println("Product listed successfully");
    }

    //Test to find products with invalid token
    @Test
    public void searchProductsWithInvalidToken() {

        given()
                .contentType("application/json")
                .header("Authorization", "Bearer teste");
        when()
                .get("/auth/products")
        .then()
                .statusCode(401)
                .body("message", is("invalid token"));
        System.out.println("Invalid or expired token");
    }

    //Test to add new product
    @Test
    public void addNewProduct() {

        String requestBody = NEW_PRODUCT;

        given()
                .contentType("application/json")
                .body(requestBody)
        .when()
                .post("/products/add")
        .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("title", equalTo("Playstation 5"))
                .body("price", equalTo(3500))
                .body("stock", equalTo(65))
                .body("rating", equalTo(4.26f))
                .body("description", equalTo("Video Game Sony Playstation 5..."))
                .body("description", equalTo("Mega Discount, Impression of A..."))
                .body("description", equalTo("Video Game Sony Playstation 5..."));
    }

    //Test to get all products
    @Test
    public void getAllProducts() {

        given()
        .when()
            .get("/products")
        .then()
                .statusCode(200)
                .body("products", hasSize(30))
                .body("total", equalTo(100));
    }

    //Test to get random product by id
    @Test
    public void getProductById() {

        List<Integer> allProductIds = given()
            .when()
                 .get("/products")
            .then()
                .statusCode(200)
            .extract()
                .jsonPath()
                .getList("products.id");

       int randomProductId = allProductIds.get(new Random().nextInt(allProductIds.size()));

       given()
            .when()
                .get("/products/" + randomProductId)
            .then()
                .statusCode(200)
                .body("id", equalTo(randomProductId));
    }

    //Test to get non-existent product
    @Test
    public void getInvalidProductById() {

        given()
                .when()
                    .get("/products/0")
                .then()
                    .statusCode(404)
                    .body("message", is("Product with id '0' not found"));
    }
}