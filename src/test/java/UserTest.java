import br.testApi.BaseTest;
import org.junit.Test;


import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class UserTest extends BaseTest{

    //Test to validate search users
    @Test
    public  void getUsers(){

        given()
                .when()
                    .get("/users")
                .then()
                    .statusCode(200)
                    .body("users", hasSize(30))
                    .body("users[0].username", notNullValue())
                    .body("users[0].password", notNullValue());
    }
}
