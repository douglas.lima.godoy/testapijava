import br.testApi.BaseTest;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class LoginTest extends BaseTest {

    //Login successfully
    public String loginSuccessfully() {

        Map<String, String> login = new HashMap<String, String>();
        login.put("username", USERNAME);
        login.put("password", PASSWORD);

        String token = given()
                    .contentType("application/json")
                    .body(login)
                .when()
                    .post("/auth/login")
                .then()
                    .statusCode(200)
                    .body("id", notNullValue())
                . extract().
                    path("token");
        return token;
    }

    //Trying login with incorrect credentials
    @Test
    public void loginIncorrectCredential() {

        Map<String, String> login = new HashMap<String, String>();
        login.put("username", "teste@teste.com");
        login.put("password", "teste");
        given()
                .contentType("application/json")
                .body(login)
                .when()
                .post("/auth/login")
                .then()
                .statusCode(400)
                .body("message", is("Invalid credentials"));
    }

    //Trying login with correct credentials
    @Test
    public void loginCorrectCredential() {

        Map<String, String> login = new HashMap<String, String>();
        login.put("username", USERNAME);
        login.put("password", PASSWORD);

        given()
                .contentType("application/json")
                .body(login)
        .when()
                .post("/auth/login")
        .then()
                .statusCode(200)
                .body("id", notNullValue());
    }
}
