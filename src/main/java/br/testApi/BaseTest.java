package br.testApi;

import org.junit.BeforeClass;
import org.junit.AfterClass;

import io.restassured.RestAssured;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;


public class BaseTest implements Constant {

    @BeforeClass
    public static void setup() {

        RestAssured.baseURI = APP_BASE_URL;
        RestAssured.filters(new AllureRestAssured());

    }
}